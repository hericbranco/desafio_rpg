<div id="sidebar">

	<div class="box">
		<div class="h_title">&#8250;&nbsp; Arma</div>
		<ul>
			<li class=""><a href="<?php echo site_url('arma') ?>">Listar</a></li>
			<li class="b2"><a href="<?php echo site_url('arma/adicionar') ?>">Adicionar</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250;&nbsp; Raça</div>
		<ul>
			<li class=""><a href="<?php echo site_url('raca') ?>">Listar</a></li>
			<li class="b2"><a href="<?php echo site_url('raca/adicionar') ?>">Adicionar</a></li>
		</ul>
	</div>
	<div class="box">
		<div class="h_title">&#8250;&nbsp; Combate</div>
		<ul>
			<li class=""><a href="<?php echo site_url('combate') ?>">Realizar Combate</a></li>
		</ul>
	</div>
	<!--
	<div class="box">
		<div class="h_title">&#8250;&nbsp; Comunicação Interna</div>
		<ul>
			<li class="b1"><a href="">Notícias</a></li>
			<li class="b2"><a href="">Jornal Espaço D'Or</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250;&nbsp; Controle Institucional</div>
		<ul>
			<li class="b1"><a href="">Patrimônio</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250;&nbsp; Financeiro</div>
		<ul>
			<li class="b1"><a href="">Centro de Custos</a></li>
			<li class="b2"><a href="">Natureza</a></li>
			<li class="b1"><a href="">Documentos</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250;&nbsp; Jurídico</div>
		<ul>
			<li class="b1"><a href="">Documentos</a></li>
			<li class="b2"><a href="">Links ou Arquivos</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250;&nbsp; Marketing</div>
		<ul>
			<li class="b1"><a href="">Clippings</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250;&nbsp; Obras</div>
		<ul>
			<li class="b1"><a href="">Obras Maiores</a></li>
			<li class="b2"><a href="">Obras Menores</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250;&nbsp; RH</div>
		<ul>
			<li class="b1"><a href="">Colaboradores</a></li>
			<li class="b2"><a href="">Gestores</a></li>
			<li class="b1"><a href="">Config. Unidades</a></li>
			<li class="b2"><a href="">Config. Setores</a></li>
			<li class="b1"><a href="">Config. Funções</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250;&nbsp; TI</div>
		<ul>
			<li class="b1"><a href="">Dashboard de Operações</a></li>
			<li class="b2"><a href="">Projetos</a></li>
			<li class="b1"><a href="">Formulários de Solicitações de Recursos</a></li>
			<li class="b2"><a href="">Padrões e Processos de TI</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250;&nbsp; Geral</div>
		<ul>
			<li class="b1"><a href="">Links Úteis</a></li>
			<li class="b2"><a href="">Páginas Informativas</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250;&nbsp; Configurações</div>
		<ul>
			<li class="b1"><a href="">Usuários do Admin</a></li>
		</ul>
	</div>

	<!--div class="box">
		<div class="h_title">&#8250; Manage content</div>
		<ul>
			<li class="b1"><a class="icon page" href="">Show all pages</a></li>
			<li class="b2"><a class="icon add_page" href="">Add new page</a></li>
			<li class="b1"><a class="icon photo" href="">Add new gallery</a></li>
			<li class="b2"><a class="icon category" href="">Categories</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250; Users</div>
		<ul>
			<li class="b1"><a class="icon users" href="">Show all users</a></li>
			<li class="b2"><a class="icon add_user" href="">Add new user</a></li>
			<li class="b1"><a class="icon block_users" href="">Lock users</a></li>
		</ul>
	</div>

	<div class="box">
		<div class="h_title">&#8250; Settings</div>
		<ul>
			<li class="b1"><a class="icon config" href="">Site configuration</a></li>
			<li class="b2"><a class="icon contact" href="">Contact Form</a></li>
		</ul>
	</div-->

	<?php /* echo getMenuForUser(getUserData('id'));*/ ?>

</div>


