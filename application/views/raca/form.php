<div class="h_title"><?php echo $title; ?> <a href="javascript:history.back(1);" class="btn-voltar">&#8249; voltar</a></div>
<?php if (!isset($id)): ?><h2>Adicionar</h2><?php endif; ?>
<form action="<?php echo site_url($slug.'/save') ?>" method="POST"enctype="multipart/form-data" >

	<?php if (isset($id)): ?><input type="hidden" name="id" id="id" value="<?php echo $id ?>" readonly="readonly" /> <?php endif; ?>

	<div class="element">
		<div class="col-1">
			<label for="contract">Nome: <span class="red">(obrigatório)</span></label>
			<input id="nome" name="nome"  value="<?php echo $nome; ?>" />
		</div>
		<div class="col-1">
			<div class="sub-element">
				<label for="vida">Pontos de Vida: <span class="red">(obrigatório)</span></label>
				<input type="text" id="vida" name="vida" value="<?php echo $vida; ?>" class="numeric" />
			</div>
			<div class="sub-element">
				<label for="forca">Força: <span class="red">(obrigatório)</span></label>
				<input type="text" id="forca" name="forca" value="<?php echo $forca; ?>" class="numeric" />
			</div>
			<div class="sub-element">
				<label for="agilidade">Agilidade: <span class="red">(obrigatório)</span></label>
				<input type="text" id="agilidade" name="agilidade" value="<?php echo $agilidade; ?>" class="numeric" />
			</div>
		</div>


	</div>

	<div class="element btns">
		<button type="submit" class="add">Salvar</button>
		<a href="javascript:history.back(1);" class="btn cancel">Cancelar</a>
		<?php if (isset($id)): ?> <a href="<?= site_url('/'.$slug."/deletar/".$id); ?>" class="btn excluir">Excluir</a> <?php endif; ?>
	</div>
</form>