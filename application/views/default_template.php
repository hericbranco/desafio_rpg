<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Desafio RPG</title>

		<?php echo getCss(); ?>

		<script type='text/javascript'>
			site_url = "<?= site_url().'/' ?>";
			base_url = site_url;
			year = "<?= date('Y') ?>";
		</script>

		<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
		<?php echo getJs(); ?>
		<script type="text/javascript">

			$(function() {
				$("#sidebar .box").find('ul').not($('.pai-open').parents('ul')).hide();
				$("#sidebar .box .h_title").click(function() {
					$(this).toggleClass('open').next("ul").stop().slideToggle();
				});
				$('.pai-open').parents('.box').children('.h_title').addClass('open');
			});

			tinyMCE.init({
				mode: "textareas",
				theme: "advanced",
				plugins: "advimage,inlinepopups",
				editor_deselector: 'notTNMC',
				theme_advanced_buttons1: "image,bold,italic,underline,separator,justifyleft,justifycenter,justifyright,separator,styleselect,separator,bullist,numlist,separator,link,unlink,separator,removeformat",
				style_formats: [
					{title: 'Subtítulo 1', block: 'h2'},
					{title: 'Subtítulo 2', block: 'h3'}
				],
				inline_styles: false,
				relative_urls: false,
				content_css: "",
				convert_urls: false,
				//file_browser_callback : 'myCustomFileBrowser'
			});

			function myCustomFileBrowser(field_name, url, type, win) {
				// Do custom browser logic
				//win.document.forms[0].elements[field_name].value = 'my browser value';
				javascript:mcTabs.displayTab('upload_tab', 'upload_panel');
			}
		</script>
	</head>
	<body>
		<?php if (isset($header) && $header == 'no-header'): ?>

		<?php else: ?>
			<?php $this->load->view('include/header'); ?>
		<?php endif; ?>

		<div id="content">

			<?php $this->load->view('include/menu'); ?>

			<div id="main">
				<?php $this->load->view(strtolower($module).'/'.$method_name); ?>
			</div>

		</div>

		<div class="clear"></div>
		<?php $this->load->view('include/footer'); ?>

	</body>
</html>