<div class="h_title"></div>
<?php if (isset($_GET['msg']) && $_GET['msg'] == 'save_success'): ?>
	<div class="n_ok"><p>Norma salva com sucesso!</p></div>
<?php elseif (isset($_GET['msg']) && $_GET['msg'] == 'save_error'): ?>
	<div class="n_error"><p>Erro</p></div>
<?php elseif (isset($_GET['msg']) && $_GET['msg'] == 'delete_success'): ?>
	<div class="n_ok"><p>Norma excluída com sucesso!</p></div>
<?php endif; ?>
<div class="element btns top">
	<h2 class="titulo"><?php echo $title ?></h2>
	<a href="<?php echo site_url($slug.'/adicionar') ?>" class="btn add">Adicionar <?php echo $title_single; ?></a>
</div>

<div class="sep"></div>
<table>
	<tr>
		<th scope="col" width="" style="text-align:left">Nome</th>
		<th scope="col">Ataque</th>
		<th scope="col">Defesa</th>
		<th scope="col">Dano</th>
		<th scope="col" width="5%">A&ccedil;&otilde;es</th>
	</tr>
</thead>

<tbody>
	<?php foreach ($arrObjDados AS $objDados): ?>
		<tr>
			<td><a href="<?= site_url('/'.$slug."/atualizar/".$objDados->id); ?>"><?php echo $objDados->nome ?></a></td>
			<td style="text-align:center"><?php echo $objDados->ataque ?></td>
			<td style="text-align:center"><?php echo $objDados->defesa ?></td>
			<td style="text-align:center"><?php echo $objDados->dano ?></td>
			<td>
				<a href="<?= site_url('/'.$slug."/atualizar/".$objDados->id); ?>" class="table-icon edit" title="Editar"></a>
				<a href="<?= site_url('/'.$slug."/deletar/".$objDados->id); ?>" class="table-icon delete" title="Deletar"></a>
			</td>
		</tr>
	<?php endforeach; ?>
</tbody>
</table>

<?php if (isset($pagination)): ?>
	<div class="sep"></div>
	<div class="entry">
		<div class="pagination">
			<?php echo $pagination; ?>
		</div>
	</div>
<?php endif; ?>