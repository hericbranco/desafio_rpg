<div class="h_title"><?php echo $title; ?> <a href="javascript:history.back(1);" class="btn-voltar">&#8249; voltar</a></div>
<?php if (!isset($id)): ?><h2>Adicionar</h2><?php endif; ?>
<form action="<?php echo site_url($slug.'/save') ?>" method="POST"enctype="multipart/form-data" >

	<?php if (isset($id)): ?><input type="hidden" name="id" id="id" value="<?php echo $id ?>" readonly="readonly" /> <?php endif; ?>

	<div class="element">
		<div class="col-1">
			<label for="nome">Nome: <span class="red">(obrigatório)</span></label>
			<input id="nome" name="nome"  value="<?php echo $nome; ?>" />
		</div>
		<div class="col-1">
			<div class="sub-element">
				<label for="ataque">Ataque: <span class="red">(obrigatório)</span></label>
				<input type="text" id="ataque" name="ataque" value="<?php echo $ataque; ?>" class="numeric" />
			</div>
			<div class="sub-element">
				<label for="defesa">Defesa: <span class="red">(obrigatório)</span></label>
				<input type="text" id="defesa" name="defesa" value="<?php echo $defesa; ?>" class="numeric" />
			</div>
			<div class="sub-element">
				<label for="ser3">Dano: <span class="red">(obrigatório)</span></label>
				<div class="sub-element"><input type="text" id="dano.qtd" name="dano[qtd]" value="<?php echo $dano['qtd']; ?>" class="numeric" /></div>
				<div class="sub-element"><?php echo form_dropdown('dano[dado]', $this->config->item('arrDados'), 'd'.$dano['dado'], 'style="margin-left:10px;" id="dano.dado"'); ?></div>

			</div>
		</div>


	</div>

	<div class="element btns">
		<button type="submit" class="add">Salvar</button>
		<a href="javascript:history.back(1);" class="btn cancel">Cancelar</a>
		<?php if (isset($id)): ?> <a href="<?= site_url('/'.$slug."/deletar/".$id); ?>" class="btn excluir">Excluir</a> <?php endif; ?>
	</div>
</form>