<script>
	$(document).ready(function () {
		$('#txt_resultado').scrollTop($('#txt_resultado').find('div').css('height').substring(0, $('#txt_resultado').find('div').css('height').length - 2));
	});
</script>

<div class="h_title"><?php echo $title; ?> <a href="javascript:history.back(1);" class="btn-voltar">&#8249; voltar</a></div>
<?php if (!isset($id)): ?><h2>Combate</h2><?php endif; ?>
<form action="<?php echo site_url($slug.'/executar_combate') ?>" method="POST"enctype="multipart/form-data" >

	<?php if (isset($id)): ?><input type="hidden" name="id" id="id" value="<?php echo $id ?>" readonly="readonly" /> <?php endif; ?>



	<div class="element">

		<div class="col-1">
			<?php foreach ($arrPersonagem AS $i => $arrAtributos): ?>
				<div class="sub-element">
					<h3>Personagem <?php echo $i ?></h3>
					<p>
						<label for="personagem<?php echo $i; ?>.nome">Nome: <span class="red">(obrigatório)</span></label>
						<input id="nome" name="personagem[<?php echo $i ?>][nome]"  value="<?php echo $arrAtributos['nome'] ?>" />
					</p>
					<p>
						<label for="personagem<?php echo $i; ?>.raca">Raça: <span class="red">(obrigatório)</span></label>
						<?php echo form_dropdown('personagem['.$i.'][raca]', $arrAtributos['arrRaca'], $arrAtributos['raca'], 'id="personagem'.$i.'.raca"'); ?>
					</p>
					<p>
						<label for="personagem<?php echo $i; ?>.raca">Arma: <span class="red">(obrigatório)</span></label>
						<?php echo form_dropdown('personagem['.$i.'][arma]', $arrAtributos['arrArma'], $arrAtributos['arma'], 'id="personagem'.$i.'.arma"'); ?>
					</p>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="col-1" style="text-align: center;">
			<div class="sub-element"><button type="submit">Executar Combate</button></div>
		</div>
		<?php if (isset($msg)): ?>
			<div  id="txt_resultado" class="col-1" style="height: 200px;overflow: scroll;text-align: center;">
				<div>
					<?php echo $msg ?>
				</div>
			</div>
		<?php endif; ?>

		<div class="element btns">

		</div>
</form>