<?php

/*
 * Configuração padrão de css e js
 */
//$config['css']	= array('grid1260', 'js/libraries/jquery-ui/css/overcast/jquery-ui-1.8.16.custom', 'main');
//$config['css']	=	array('base', 'skeleton', 'layout', 'libs/jquery-impromptu/jquery-impromptu');
//$config['js']	=	array('libs/modernizr-2.5.2.min', 'number-polyfill', 'libs/jquery-impromptu/jquery-impromptu', 'tools');

$config['css'] = array('style', 'navi', 'js/libs/jquery-ui-1.9.0.custom/css/pepper-grinder/jquery-ui-1.9.0.custom');
$config['js'] = array('jquery-1.7.2.min', 'libs/jquery.maskedinput/jquery.maskedinput.min', 'tiny_mce/tiny_mce', 'libs/jquery-ui-1.9.0.custom/js/jquery-ui-1.9.0.custom', 'main');
//$config['js']	= array('libraries/jquery/jquery', 'libraries/jquery-ui/js/jquery-ui-1.8.16.custom.min', 'libraries/jquery-ui/development-bundle/ui/i18n/jquery-ui-i18n', 'libraries/jquery-maskedinput/jquery.maskedinput-1.3', 'main');


/*
 * Configurações específicas de cada página
 * Ex.: $config['css']['controller']['method'] = array('teste');
 */

$config['js']['arma']['index'] = array('confirm-delete');
$config['js']['raca']['index'] = array('confirm-delete');


$config['js']['combate']['index'] = array('valida-form');
$config['js']['arma']['form'] = array('valida-form');
$config['js']['raca']['form'] = array('valida-form');

?>