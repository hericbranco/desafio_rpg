<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Combate extends CI_Controller {
	public function index(array $arrFieldsValues = null) {

		$arrVar['title'] = $this->lang->line('title');
		$arrVar['title_single'] = $this->lang->line('title_single');
		$arrVar['slug'] = 'combate';
		$arrVar['module'] = get_class($this);
		$arrVar['method_name'] = __FUNCTION__;

		$arrVar['arrPersonagem'] = array();
		for ($i = 1; $i <= 2; $i++) {
			$this->load->model('raca_model');
			$arrObjUnidade = $this->raca_model->getBy(array(), array());
			if (!empty($arrObjUnidade)) {
				$arrRaca = array('' => 'Selecione');
				foreach ($arrObjUnidade AS $objUnidade) {
					$arrRaca[$objUnidade->id] = $objUnidade->nome;
				}
			}

			$this->load->model('arma_model');
			$arrObjUnidade = $this->arma_model->getBy(array(), array());
			if (!empty($arrObjUnidade)) {
				$arrArma = array('' => 'Selecione');
				foreach ($arrObjUnidade AS $objUnidade) {
					$arrArma[$objUnidade->id] = $objUnidade->nome;
				}
			}
			$arrVar['arrPersonagem'][$i] = array('arrArma' => $arrArma, 'arrRaca' => $arrRaca);
			if (isset($arrFieldsValues['personagem'][$i])) {
				$arrVar['arrPersonagem'][$i]['nome'] = $arrFieldsValues['personagem'][$i]['nome'];
				$arrVar['arrPersonagem'][$i]['raca'] = $arrFieldsValues['personagem'][$i]['raca'];
				$arrVar['arrPersonagem'][$i]['arma'] = $arrFieldsValues['personagem'][$i]['arma'];
			} else {
				$arrVar['arrPersonagem'][$i]['nome'] = "";
				$arrVar['arrPersonagem'][$i]['raca'] = "";
				$arrVar['arrPersonagem'][$i]['arma'] = "";
			}
		}

		if (isset($arrFieldsValues['msg'])) {
			$arrVar['msg'] = $arrFieldsValues['msg'];
		}


		$this->load->view('default_template', $arrVar);
	}

	public function executar_combate() {
		$arrValues = $this->input->post();

		foreach ($arrValues['personagem'] AS $i => $arrAtt) {
			$this->load->model('raca_model');
			$arrObjRaca = $this->raca_model->getById(array('id' => $arrAtt['raca']));

			$this->load->model('arma_model');
			$arrObjArma = $this->arma_model->getById(array('id' => $arrAtt['arma']));

			$arrPersonagem[] = array(
				"nome" => $arrAtt['nome'],
				"raca" => $arrObjRaca['0']->nome,
				"forca" => $arrObjRaca['0']->forca,
				"agilidade" => $arrObjRaca['0']->agilidade,
				"vida" => $arrObjRaca['0']->vida,
				"ataque" => $arrObjArma['0']->ataque,
				"defesa" => $arrObjArma['0']->defesa,
				"dano" => $arrObjArma['0']->dano,
				"iniciativa" => 0
			);
		}

		$msg = '';

		while ($arrPersonagem['0']['vida'] > 0 || $arrPersonagem['1']['vida'] > 0) {
			$msg .= "<p>";
			$arr_ordem_turno = array('0' => array("atacante" => 0, "defensor" => 1), '1' => array("atacante" => 1, "defensor" => 0));

			while ($arrPersonagem['0']["iniciativa"] == $arrPersonagem['1']["iniciativa"]) {
				$arrPersonagem['0']["iniciativa"] = $this->_calcula_iniciativa($arrPersonagem['0']);
				$arrPersonagem['1']["iniciativa"] = $this->_calcula_iniciativa($arrPersonagem['1']);
				$msg .= 'Rolando iniciativa '.$arrPersonagem['0']['nome'].'('.$arrPersonagem['0']['raca'].') ('.$arrPersonagem['0']['iniciativa'].') '.$arrPersonagem['1']['nome'].'('.$arrPersonagem['1']['raca'].') ('.$arrPersonagem['1']['iniciativa'].')<br/>';
			}

			if ($arrPersonagem['1']["iniciativa"] > $arrPersonagem['0']["iniciativa"]) {
				$arr_ordem_turno = array('0' => array("atacante" => 1, "defensor" => 0), '1' => array("atacante" => 0, "defensor" => 1));
			}

			foreach ($arr_ordem_turno AS $arrCombate) {
				$msg .= $arrPersonagem[$arrCombate['atacante']]['nome'].'('.$arrPersonagem[$arrCombate['atacante']]['raca'].") Tenta Atacar ".$arrPersonagem[$arrCombate['defensor']]['nome'].'('.$arrPersonagem[$arrCombate['defensor']]['raca'].")";

				if ($this->_calcula_chance_ataque($arrPersonagem[$arrCombate['atacante']]) > $this->_calcula_chance_defesa($arrPersonagem[$arrCombate['defensor']])) {
					$dano = $this->_calcula_dano($arrPersonagem[$arrCombate['atacante']]);
					$arrPersonagem[$arrCombate['defensor']]["vida"] = $arrPersonagem[$arrCombate['defensor']]["vida"] - $dano;
					$msg .= " e  Causa ".$dano." de dano<br/>";
				} else {
					$msg .= " sem sucesso<br/>";
				}

				if ($arrPersonagem[$arrCombate['defensor']]["vida"] <= 0) {
					break;
				}
			}
			$arrPersonagem['0']['iniciativa'] = 0;
			$arrPersonagem['1']['iniciativa'] = 0;
			$msg .= 'Os pontos de vida de '.$arrPersonagem['0']['nome'].'('.$arrPersonagem['0']['raca'].") sao ".$arrPersonagem['0']['vida']."<br/>";
			$msg .= 'Os pontos de vida de '.$arrPersonagem['1']['nome'].'('.$arrPersonagem['1']['raca'].") sao ".$arrPersonagem['1']['vida']."<br/>";
			if ($arrPersonagem['0']['vida'] <= 0 || $arrPersonagem['1']['vida'] <= 0) {
				break;
			}
			$msg .= "</p>";
		}
		$msg .= "<p>";
		if ($arrPersonagem['0']['vida'] > $arrPersonagem['1']['vida']) {
			$msg .= $arrPersonagem['0']['nome'].'('.$arrPersonagem['0']['raca'].") vencedor";
		} else {
			$msg .= $arrPersonagem['1']['nome'].'('.$arrPersonagem['1']['raca'].") vencedor";
		}
		$msg .= "</p>";

		$this->index(array_merge($arrValues, array('msg' => $msg)));
	}

	private function _calcula_dano(array $arrPersonagem) {
		return $this->_calcula_dados($arrPersonagem["dano"]) + $arrPersonagem["forca"];
	}

	private function _calcula_chance_ataque(array $arrPersonagem) {
		return $this->_calcula_dados("1d20") + $arrPersonagem["agilidade"] + $arrPersonagem["ataque"];
	}

	private function _calcula_chance_defesa(array $arrPersonagem) {
		return $this->_calcula_dados("1d20") + $arrPersonagem["agilidade"] + $arrPersonagem["defesa"];
	}

	private function _calcula_iniciativa(array $arrPersonagem) {
		return $this->_calcula_dados("1d20") + $arrPersonagem["agilidade"];
	}

	private function _calcula_dados($dado) {
		list($qtd, $max) = explode("d", $dado);
		$valor = 0;
		for ($i=1;$i<=$qtd;$i++) {
			$valor +=$valor+rand(1, $max);
		}
		return $valor;
	}

}
