<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Arma extends My_Controller {
	public function index($offset = 0) {
		$arrVar['title'] = $this->lang->line('title');
		$arrVar['title_single'] = $this->lang->line('title_single');
		$arrVar['slug'] = 'arma';
		$arrVar['module'] = get_class($this);
		$arrVar['method_name'] = __FUNCTION__;

		$per_page = $this->config->item('per_page');

		$this->load->model('arma_model', 'model');
		$num_result = $this->model->getTotalBy($this->_stringUrlToArray($this->input->get(), array()));
		$arrVar['arrObjDados'] = $this->model->getBy($this->_stringUrlToArray($this->input->get(), array()), array('order by' => 'updated DESC', 'limit' => $offset.', '.$per_page));

		if ($num_result > $per_page) {
			$this->load->library('pagination');
			$arrConfig['base_url'] = base_url().$this->config->item('index_page').'/'.get_class($this).'/index';
			$arrConfig['total_rows'] = $num_result;
			$arrConfig['per_page'] = $per_page;
			$arrConfig['uri_segment'] = 3;
			$arrConfig['suffix'] = $this->_returnQueryStringUrl($this->input->get(), array('login', 'profile_id'));
			$arrConfig['first_url'] = $arrConfig['base_url'].$arrConfig['suffix'];
			$this->pagination->initialize($arrConfig);

			$arrVar['pagination'] = $this->pagination->create_links();
		}




		$this->load->view('default_template', $arrVar);
	}

	public function update($id) {
		try {

			$this->load->model('arma_model', 'model');
			$arrObjDados = $this->model->getById(array('id' => $id));

			$this->form(array(
				'id' => $arrObjDados[0]->id,
				'nome' => $arrObjDados[0]->nome,
				'ataque' => $arrObjDados[0]->ataque,
				'defesa' => $arrObjDados[0]->defesa,
				'dano' => $arrObjDados[0]->dano));
		} catch (exception $e) {
			$this->Logger->error($e->getMessage()."\n".$e->getTraceAsString());
		}
	}

	public function form(array $arrFieldsValues = null) {
		$arrVar['title'] = $this->lang->line('title');
		$arrVar['module'] = get_class($this);
		$arrVar['slug'] = 'arma';
		$arrVar['method_name'] = __FUNCTION__;

		$arrVar['permissao'] = 'edit';
		if (is_array($arrFieldsValues)) {
			if (isset($arrFieldsValues['id'])) {
				$arrVar['id'] = $arrFieldsValues['id'];
			}

			$arrVar['nome'] = $arrFieldsValues['nome'];
			$arrVar['ataque'] = $arrFieldsValues['ataque'];
			$arrVar['defesa'] = $arrFieldsValues['defesa'];
			list($arrVar['dano']['qtd'], $arrVar['dano']['dado']) = explode('d', $arrFieldsValues['dano']);
		} else {
			$arrVar['nome'] = "";
			$arrVar['ataque'] = "";
			$arrVar['defesa'] = "";
			$arrVar['dano'] = array('qtd' => '', 'dado' => '');
		}



		$this->load->view('default_template', $arrVar);
	}

	public function save() {
		$this->load->model('arma_model', 'model');

		$arrValues = $this->input->post();

		if (is_array($arrValues['dano'])) {
			$arrValues['dano'] = $arrValues['dano']['qtd'].$arrValues['dano']['dado'];
		}

		if ($this->input->post('id')) {
			$action_return = $this->model->update($arrValues, array('id' => $this->input->post('id')));
		} else {
			$action_return = $this->model->insert($arrValues);
		}

		if ($action_return) {
			$msg = 'save_success';
		} else {
			$msg = 'save_error';
		}

		redirect('/'.get_class($this).'?msg='.$msg, 'refresh');
	}

	public function delete($id) {


		$this->load->model('arma_model', 'model');

		if ($this->model->delete(array('id' => $id))) {
			$msg = 'delete_success';
		} else {
			$msg = 'delete_error';
		}
		redirect('/'.get_class($this).'?msg='.$msg, 'refresh');
	}

}
