<?php

function assets_url() {
	$CI = & get_instance();
	return $CI->config->item('assets');
}

function getArrUrl() {
	$CI = & get_instance();
	//echo '<pre>';print_r($CI->uri->rsegments);echo '</pre>';
	$arrUrl = explode('/', str_replace($CI->config->item('base_url'), '', current_url()));
	//echo '<pre>';print_r($arrUrl);echo '</pre>';
	$arrUrl = $CI->uri->rsegments;
	if ($CI->config->item('index_page') == '') {
		$controller = $arrUrl[0];
		$method = (isset($arrUrl[1])) ? $arrUrl[1] : '';
	} else {
		if (count($arrUrl) == 1) {
			$controller = 'standard';
			$method = 'index';
		} else {
			$controller = $arrUrl[1];
			$method = (isset($arrUrl[2])) ? $arrUrl[2] : '';
		}
	}
	return array('controller' => $controller, 'method' => $method);
}

function getCss() {
	$CI = & get_instance();
	$arrCss = $CI->config->item('css');
	if (!empty($arrCss)) {
		$return = '';
		foreach ($arrCss AS $indice => $fileName) {
			if (!is_array($fileName)) {
				if (substr($fileName, 0, 2) == 'js') {
					$return .= '<link rel="stylesheet" type="text/css" media="all" href="'.str_replace("index.php", "", site_url()).$CI->config->item('assets').$fileName.'.css" />'."\n";
				} else {
					$return .= '<link rel="stylesheet" type="text/css" media="all" href="'.str_replace("index.php", "", site_url()).$CI->config->item('assets').'css/'.$fileName.'.css" />'."\n";
				}
			} else {
				$controller = $indice;
				$arrUrl = getArrUrl();
				foreach ($fileName AS $indice => $fileName) {
					if (is_numeric($indice)) {
						$method = '';
					} else {
						$method = $indice;
					}

					if ($arrUrl['controller'] == $controller) {
						if ($method == '') {
							$return .= '<link rel="stylesheet" type="text/css" media="all" href="'.str_replace("index.php", "", site_url()).$CI->config->item('assets').'css/'.$fileName.'.css" />'."\n";
						} elseif ($arrUrl['method'] == $method) {
							foreach ($fileName AS $fileName) {
								$return .= '<link rel="stylesheet" type="text/css" media="all" href="'.str_replace("index.php", "", site_url()).$CI->config->item('assets').'css/'.$fileName.'.css" />'."\n";
							}
						}
					}
				}
			}
		}
		return $return;
	}
}

function getJs() {
	$CI = & get_instance();
	$arrJs = $CI->config->item('js');
	if (!empty($arrJs)) {
		$return = '';
		foreach ($arrJs AS $indice => $fileName) {
			if (!is_array($fileName)) {
				$return .= '<script type="text/javascript" src="'.str_replace("index.php", "", site_url()).$CI->config->item('assets').'js/'.$fileName.'.js"></script>'."\n";
			} else {
				$controller = $indice;
				$arrUrl = getArrUrl();
				foreach ($fileName AS $indice => $fileName) {
					if (is_numeric($indice)) {
						$method = '';
					} else {
						$method = $indice;
					}

					if ($arrUrl['controller'] == $controller) {
						if ($method == '') {
							$return .= '<script type="text/javascript" src="'.str_replace("index.php", "", site_url()).$CI->config->item('assets').'js/'.$fileName.'.js"></script>'."\n";
						} elseif ($arrUrl['method'] == $method) {
							foreach ($fileName AS $fileName) {
								$return .= '<script type="text/javascript" src="'.str_replace("index.php", "", site_url()).$CI->config->item('assets').'js/'.$fileName.'.js"></script>'."\n";
							}
						}
					}
				}
			}
		}
		return $return;
	}
}

?>