<?php

class MY_Model extends CI_Model {

	protected $_DB;
	protected $Logger;
	protected $_tableName;

	public function __toString() {
		return substr(get_class($this), 0, -6);
	}

	/*
	 * Metodo construtor que instancia o log e o banco de dados
	 *
	 */
	public function __construct() {
		parent::__construct();

		$this->_tableName = strtolower(substr(get_class($this), 0, -6));

		//$this->_tableName = substr(get_class($this), 0, -6);

		$this->_DB = $this->load->database('default', true);
	}

	public function setTableName($tableName) {
		echo $tableName;
		$this->_tableName = $tableName;
	}

	public function delete(array $arrTerms) {
		return $this->update(array('status' => '0'), $arrTerms);
	}

	public function save(array $arrValues) {
		if (isset($arrValues['id']) && $arrValues['id'] != '') {
			$return = $this->update($arrValues, array('id' => $arrValues['id']));
		} else {
			$return = $this->insert($arrValues);
		}
		return $return;
	}

	public function update(array $arrValues, array $arrTerms) {
		$sql = "UPDATE ".$this->_tableName." SET ";
		foreach ($arrValues AS $field => $value) {
			$sql .= " ".$field."= ? ,";
		}

		$condition = '';
		foreach ($arrTerms AS $field => $value) {
			$condition .= " ".$field."= ".$value." AND";
		}
		$condition = substr($condition, 0, -3);

		$sql = substr($sql, 0, -1)." WHERE ".$condition;

		$this->_DB->trans_start();
		$result = $this->_DB->query($sql, $arrValues);
		//echo $this->_DB->last_query().'<br/>';exit;
		//$this->_DB->trans_rollback();
		$this->_DB->trans_complete();

		if ($this->_DB->trans_status() === TRUE) {
			return true;
		}
	}

	/*
	 * Método default para inserts
	 * @param array $arrValues - array com os dados do insert('field' => 'value', ...)
	 * @return int $id - Id da última inserção
	 */
	public function insert(array $arrValues) {

		$string_parameter = '';
		foreach ($arrValues AS $field => $value) {
			if ($value != '') {
				$string_parameter .= '?, ';
			} else {
				unset($arrValues[$field]);
			}
		}

		$sql = 'INSERT INTO '.$this->_tableName.'('.implode(', ', array_keys($arrValues)).') VALUES ('.substr($string_parameter, 0, -2).')';
		$this->_DB->trans_start();
		$result = $this->_DB->query($sql, $arrValues);
		$last_id = $this->_DB->insert_id();

		//echo $this->_DB->last_query();exit;
		//echo $this->_DB->_error_message();
		//$this->_DB->trans_rollback();
		$this->_DB->trans_complete();

		if ($this->_DB->trans_status() === true) {
			return $last_id;
		}
	}

	public function getById(array $arrValues) {
		return $this->getBy(array('id' => $arrValues['id']), array());
	}

	public function getBy(array $arrValues, array $arrOptions) {

		$sql = 'SELECT * FROM '.$this->_tableName;
		if (!isset($arrValues['status'])) {
			$arrValues['status'] = 1;
		}
		if (!empty($arrValues)) {
			$sql .= ' WHERE';
			foreach ($arrValues AS $field => $value) {
				if (in_array($field, array('id', 'type', 'status'))) {
					if ($value == '') {
						unset($arrValues[$field]);
					} else {
						$sql .= " ".$field." = ? AND";
					}
				} else {
					if ($value == '') {
						unset($arrValues[$field]);
					} else {
						if (substr($field, 0, 2) == 'id') {
							$sql .= " ".$field." = ? AND";
						} else {
							$sql .= " ".$field." LIKE ? AND";
							$arrValues[$field] = '%'.$value.'%';
						}
					}
				}
			}
			$sql = substr($sql, 0, -4);
		}

		if (empty($arrOptions)) {
			$arrOptions = array('order by' => 'inserted');
		}

		foreach ($arrOptions AS $command => $field) {
			$sql .= ' '.$command.' '.$field;
		}

		$resultSet = $this->_DB->query($sql, $arrValues);

		/*if ($this->_DB->_error_message() != '') {
			echo $this->_DB->last_query().'<br/>';
			echo $this->_DB->_error_message();
		}*/

		return $this->_afterGetBy($resultSet->result());
	}

	protected function _resultQuery($query, $arrValues = null) {
		$result = $this->_DB->query($query, $arrValues);
		//echo $this->_DB->last_query();
		return $result->result();
	}

	protected function _afterGetBy(array $arrObjDados) {
		return $arrObjDados;
	}

	public function getTotalBy(array $arrValues) {
		$sql = "SELECT COUNT(*) AS total FROM ".$this->_tableName;

		if (!isset($arrValues['status'])) {
			$arrValues['status'] = 1;
		}

		if (!empty($arrValues)) {
			//fazer aqui
			$sql .= " WHERE";
			foreach ($arrValues AS $field => $value) {
				if (in_array($field, array('id', 'type', 'status'))) {
					if ($value == '') {
						unset($arrValues[$field]);
					} else {
						$sql .= " ".$field." = ? AND";
					}
				} else {
					if ($value == '') {
						unset($arrValues[$field]);
					} else {
						if (substr($field, 0, 2) == 'id') {
							$sql .= " ".$field." = ? AND";
						} else {
							$sql .= " ".$field." LIKE ? AND";
							$arrValues[$field] = '%'.$value.'%';
						}
					}
				}
			}
			$sql = substr($sql, 0, -4);
		}

		$resultSet = $this->_DB->query($sql, $arrValues);
		//$db_error_msg = $this->_DB->_error_message();
		//echo $this->_DB->last_query();
		$arrObj = $resultSet->result();

		return $arrObj[0]->total;

		//N�o deveria chegar aqui
		throw new Exception('Error Getting data', 500);
	}

	/*
	 * Metodo que trata se o valor de um campo e prepara o teste dele para o 'where'
	 * @param $option - valor do campo a ser tratado
	 * @return string $where_option - valor do campo tratado para melhor se adequar ao Where
	 */
	protected function _checkIsNullAndTransformOptions($option) {
		if (is_array($option)) {
			$where_return = " IN ('".implode("', '", $option)."')";
		} elseif (strtolower($option) == 'null') {
			$where_return = " IS NULL";
		} elseif ((strtolower($option) == 'not null') || (strtolower($option) == 'notnull')) {
			$where_return = " IS NOT NULL";
		} else {
			$where_return = " = '".$option."'";
		}
		return $where_return;
	}

	protected function _transformOnOffValues($values) {
		$arrTransform = array('on' => '1', 'off' => '0');
		$values = strtolower($values);
		if ($values == 'on' || $values == 'off') {
			return $arrTransform[$values];
		}
	}

	protected function _transformSenha($value) {
		return md5($value);
	}

	public function _reorderArrayBy(array $arrDados, $value_to_order) {
		$arrReturn = array();
		if (!empty($arrDados)) {
			foreach ($arrDados AS $dados) {
				if (is_array($dados)) {
					$arrReturn[$dados[$value_to_order]][] = $dados;
				} elseif (is_object($dados)) {
					$arrReturn[$dados->$value_to_order][] = $dados;
				}
			}
		}
		return $arrReturn;
	}

}

?>
