<?php

abstract class MY_Controller extends CI_Controller {

	protected $Logger;
	protected $_debug;
	public $language;
	protected $_objUser;

	public function __construct() {
		parent::__construct();
		//$this->lang->load('default_words', $this->config->item('language'));
		//$this->lang->load(get_class($this), $this->config->item('language'));
		//$this->language = $this->config->item('language');
		//$this->_checks_logged();
	}

	private function _checks_logged() {

		$arrUri = $this->uri;
		$controller = $arrUri->rsegments[1];
		$method = $arrUri->rsegments[2];
		$slug = implode('/', $arrUri->segments);
		$slug_principal = '';
		if (isset($arrUri->segments[1])) {
			$slug_principal = $arrUri->segments[1];
		}
		if ($slug_principal == 'configuracao') {
			$slug_principal .= '/'.$arrUri->segments[2];
		}

		$this->_objUser = $this->session->userdata('arrUser');

		$this->load->model('usuario_admin_model', 'MY_Controller_model');

		$arrObjAcessoFree = $this->MY_Controller_model->getAcessosForCheckBy(array('url' => $slug, 'is_public' => '1'));
		if (!empty($arrObjAcessoFree)) {
			$arrAccessFree = array();
			foreach ($arrObjAcessoFree AS $objAcessoFree) {
				$arrAccessFree[] = $objAcessoFree->url;
			}
		}
		if (!in_array($slug, $arrAccessFree) && !in_array($slug_principal, $arrAccessFree)) {
			if ($this->session->userdata('arrUser')) {
				if ($controller != 'standard') {
					$objUser = $this->session->userdata('arrUser');
					$this->load->model('usuario_admin_model', 'MY_Controller_model');
					$arrObjAcesso = $this->MY_Controller_model->getAcessosForCheckBy(array('id_usuario_admin' => $objUser->id), array());
					if (!empty($arrObjAcesso)) {
						$arrAccess = array();
						foreach ($arrObjAcesso AS $objAcesso) {
							$arrAccess[] = $objAcesso->url;
						}
					}

					if (!in_array($slug, $arrAccess) && !in_array($slug_principal, $arrAccess)) {
						redirect(site_url().'?msg=access_error');
					}
				}
			} else {
				if ($method != 'login' && $method != 'make_login') {
					redirect('/login?msg=not_logged&url='.base64_encode(current_url()), 'refresh');
				}
			}
		}
	}

	protected function _stringUrlToArray($arrQueryString, $arrValues) {
		$arrReturn = array();
		$dottedName = false;
		if (!empty($arrQueryString) && !empty($arrValues)) {
			foreach ($arrValues AS $values) {
				if (preg_match('/^[a-z]+\.[a-z_]+$/', $values)) {
					list($dotBefore, $dotAfter) = explode('.', $values);
					$field = $dotAfter;
				} else {
					$field = $values;
				}
				if (isset($arrQueryString[$field])) {
					$arrReturn[$values] = $arrQueryString[$field];
				}
			}
		}
		return $arrReturn;
	}

	public function _returnQueryStringUrl($arrQueryString, $arrValues) {
		if (!empty($arrQueryString) && !empty($arrValues)) {
			$TextReturn = '?';
			foreach ($arrValues AS $values) {
				if (isset($arrQueryString[$values])) {
					if (is_array($arrQueryString[$values])) {
						$TextReturn .= $values.'[]='.implode('&'.$values.'[]=', $arrQueryString[$values]).'&';
					} else {
						$TextReturn .= ''.$values.'='.$arrQueryString[$values].'&';
					}
				}
			}
			return substr($TextReturn, 0, -1);
		}
	}

	protected function _prepareArrayForSelect(array $arrValues, $index = null, $value = null) {
		if (!empty($arrValues)) {
			$arrReturn = array('' => 'Selecione');
			foreach ($arrValues AS $arrIndex => $arrValue) {
				if ($value == null) {
					$value_to_array = $arrValue[key($arrValue)];
				} else {
					$value_to_array = $arrValue->$value;
				}

				if ($index == null) {
					$arrReturn[$arrIndex] = $value_to_array;
				} else {
					$arrReturn[$arrValue->$index] = $value_to_array;
				}
			}
			return $arrReturn;
		}
	}

	protected function _accessRest($api_name, $type, $method, $arrValues, $returnError = 0) {
		$this->config->load('apis_settings', true);
		$this->load->library('rest', array(
			'server' => $this->config->item($api_name, 'apis_settings'),
			'http_user' => $this->config->item('api_user', 'apis_settings'),
			'http_pass' => $this->config->item('api_pass', 'apis_settings'),
			'http_auth' => $this->config->item('api_auth', 'apis_settings')
				), $api_name);
		$resultApi = $this->$api_name->$type($method, $arrValues);
		$this->Logger->debug('$resultApi: '.print_r($resultApi, true));
		$statusResponse = $this->$api_name->status();
		$this->Logger->debug('$statusResponse: '.print_r($statusResponse, true));
		$return = null;
		switch ($statusResponse) {
			case 200:
				$return = $resultApi;
				break;

			case 404:
				$this->Logger->info('Data not found');
				if ($returnError == 1) {
					$objReturn = new stdClass();
					$objReturn->status = 'notFound';
					$return = $objReturn;
				}
				break;

			default:
				$this->Logger->info('Fail occurred while trying to retrieve data');
				break;
		}
		$this->Logger->debug('$return: '.print_r($return, true));
		return object2array($return);
	}

	public function __destruct() {

	}

}

?>