$().ready(function(){
	$('button').click(function(){
		$(this).parents('form').attr('action', site_url+'cadastro/criar_senha');
		if ($(this).attr('id') == 'btn_continue') {
			if (!$('#chb_is_acept_term').is(':checked')) {
				alert('Para continuar, não se esqueça de ler Termos e Condições do nosso serviço.');
				return false;
			}
		} else {
			$(this).parents('form').attr('action', site_url+'cadastro/confirmar_numero');
		}
	});
});