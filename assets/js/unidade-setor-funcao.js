$().ready(function(){
	$default_value = 'Selecione';
	$('#id_setor, #id_funcao').attr('disabled', true);

	$('#id_unidade').change(function(){
		if ($(this).val() != '') {
			$.ajax({
				url		: base_url + 'perfil/return_setor',
				data		: 'id_unidade='+$(this).val(),
				dataType	: 'json',
				success	: function(arrObjSetor) {
					$('#id_setor').html('<option value="">'+$default_value+'</option>');
					$('#id_funcao').html('<option value="">'+$default_value+'</option>').attr('disabled', true);
					$.each(arrObjSetor, function(id_setor, nome) {
						$('<option></option>').attr('value', id_setor).html(nome).appendTo('#id_setor');
					});
					$('#id_setor').val($('#id_setor').next('input').val()).removeAttr('disabled').change();
					$('#id_setor').next('input').remove();
				}
			});
		}
	});

	$('#id_setor').change(function(){
		if ($(this).val() != '') {
			$.ajax({
				url		: base_url + 'perfil/return_funcao',
				data		: 'id_setor='+$(this).val(),
				dataType	: 'json',
				success	: function(arrObj) {
					$('#id_funcao').html('<option value="">'+$default_value+'</option>');
					$.each(arrObj, function(id, nome) {
						$('<option></option>').attr('value', id).html(nome).appendTo('#id_funcao');
					});
					$('#id_funcao').val($('#id_funcao').next('input').val()).removeAttr('disabled');
					$('#id_funcao').next('input').remove();
					$('.disabled').attr('disabled', true);
				}
			});
		}
	});

	if ($('#id_unidade').val() != '') {
		$('#id_unidade').change();
	}
});