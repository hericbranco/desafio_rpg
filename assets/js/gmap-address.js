$().ready(function(){

	$.fn.showInGmap = function($div_id) {
		$address = $(this).val();
		$("#"+$div_id).css('height', '0').html('');
		if ($address != '') {
			$.ajax({
				url			: base_url+'ajax/getLatLongByAddress',
				data		: 'address='+$address,
				dataType	: 'json',
				type		: 'POST',
				success		: function($arrLoc) {
					$("#"+$div_id).css('height', '300px').gMap({
						markers: [{
							latitude: $arrLoc.lat,
							longitude: $arrLoc.lng
						}],
						zoom: 15,
						maptype: 'ROADMAP'
					});
				}
			});
		}
	}

	$('#localizacao').blur(function(){
		$(this).showInGmap('gmap');
	});

	$('#localizacao').showInGmap('gmap');
});