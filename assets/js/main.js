$().ready(function () {

	$.datepicker.regional['pt-BR'] = {
		closeText: 'Fechar',
		prevText: '&#x3C;Anterior',
		nextText: 'Próximo&#x3E;',
		currentText: 'Hoje',
		monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
			'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
		monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun',
			'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
		dayNames: ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'],
		dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
		dayNamesMin: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb'],
		weekHeader: 'Sm',
		dateFormat: 'dd/mm/yy',
		firstDay: 0,
		isRTL: false,
		showMonthAfterYear: false,
		yearSuffix: ''
	};
	$.datepicker.setDefaults($.datepicker.regional['pt-BR']);

	$('.date').datepicker({
		changeMonth: true,
		changeYear: true,
		minDate: 'D'
	});
	/*
	 * $( "#datepicker" ).datepicker({
	 changeMonth: true,
	 changeYear: true
	 });
	 */
	//$('.tel').mask('(99)9999-9999');

	$.fn.setNumberMask = function () {
		if ($(this).not('.fixo').val() == '11') {
			//$(this).next('.tel').mask('99999-9999');
			$max = 9;
		} else {
			$max = 8;
			//$(this).next('.tel').mask('9999-9999');
		}
		$(this).next('.tel').attr('maxlength', $max);
	};

	$('.ddd').each(function () {
		$(this).setNumberMask();
	}).blur(function () {
		$(this).setNumberMask();
	});

	$('.tel').blur(function () {
		if ($(this).val().length == $(this).attr('maxlength')) {
			$(this).val($(this).val().substr(0, $(this).attr('maxlength') - 4) + '-' + $(this).val().substr($(this).attr('maxlength') - 4))
		}
	}).each(function () {
		$(this).blur()
	}).focus(function () {
		$(this).val($(this).val().replace('-', ''));
	});

	$(".numeric").keypress(function (event) {
		var tecla = (window.event) ? event.keyCode : event.which;
		if (tecla != 0 && tecla != 8 && (tecla < 48 || tecla > 57)) {
			return false;
		}
	});

});