$().ready(function(){
	$editNumber = true;
	$('#dvPassword, #dvIsTerms, .obs, .action:not(#dvBtnValidateMsisdn)').hide();

	$('#ddd').focus(function(){
		if ($editNumber == true) {
			$(this).val('').mask('(99)');
		}
	}).keyup(function(){
		if($(this).valOnlyNumber().length == 2){
			$('#number').focus();
		}
	});

	$('#number').focus(function(){
		if ($editNumber == true) {
			if ($('#ddd').val() == '') {
				$('#ddd').focus();
			}
			$(this).setNumberMask();
		}
	}).blur(function(){
		if ($('#ddd').val() != '' && $(this).valOnlyNumber() != '') {
	//$().verifyMsisdn($('#ddd'), $(this));
	}
	});

	$('#btnValidateMsisdn').click(function(){
		$().verifyMsisdn($('#ddd'), $('#number'));

	});

	$.fn.setNumberMask = function() {
		$(this).val('');
		if ($('#ddd').val() == '(11)') {
			$(this).mask('99999-9999');
		} else {
			$(this).mask('9999-9999');
		}
	};

	$.fn.valOnlyNumber = function() {
		valor = '';
		valor = $(this).val().replace('(','');
		valor = valor.replace(')','');
		valor = valor.replace(/_/g,'');
		valor = valor.replace('-','');
		return valor;
	};

	$.fn.verifyMsisdn = function($ddd, $number) {
		$msisdn = '55'+$ddd.valOnlyNumber()+$number.valOnlyNumber();
		$controller = 'signup';
		if ($ddd.valOnlyNumber() == 11) {
			$mustContain = 9;
		} else {
			$mustContain = 8;
		}

		if ($number.valOnlyNumber().length < $mustContain) {
			promptAlert('<br /><p>Veja se o número contém ' + $mustContain + ' dígitos e tente mais uma vez.</p>');
		} else {
			jQuery.ajax({
				url: base_url + $controller + '/verify_msisdn/',
				data: 'msisdn='+$msisdn,
				dataType: "json",
				success: function(verify_msisdn){
					if (verify_msisdn.message == 'canSign') {
						$('#btnValidateMsisdn').hide();
						$ddd.attr('readonly', 'true');
						$number.attr('readonly', 'true');
						$editNumber = false;
						$('#dvPassword, #dvIsTerms, .obs, .action:not(#dvBtnValidateMsisdn)').show();
					} else {
						promptAlert(verify_msisdn.message);
					}
				}
			});

		}
	};


});