$().ready(function(){
	$('#nome').blur(function(){
		$('.verify_error').remove();
		$('button[type="submit"]').removeAttr('disabled');
		if ($(this).val() != '') {
			if ($('.h_title').html().toLowerCase() == 'centro de custos') {
				$type = 'centro_de_custos';
			} else if ($('.h_title').html().toLowerCase() == 'naturezas') {
				$type = 'natureza';
			}

			$.ajax({
				url: base_url + 'informacao_generica' + '/verify_nome',
				data: 'arrValues[type]='+$type+'&arrValues[nome]='+$('#nome').val(),
				dataType: "json",
				success: function($objReturn){
					if ($objReturn.verify_number == 'exist') {
						$('form').before('<div class="n_error verify_error"><p>Já existe um registro com esse título, por favor selecione outro</p></div>');
						$('button[type="submit"]').attr('disabled', true).click(function(){$('#nome').focus();});
					} else {
						$('button[type="submit"]').removeAttr('disabled');
					}
				}
			});
		}
	});
});