$().ready(function(){

	$('.btn_add_file').click(function(){
		//$('<input type="file" name="attach[]"/>').insertBefore($(this).parent('div'));
		$last_input_name = $(this).parent('.btns').prev('input').attr('name');
		$total = $(this).parents('.sub-element').find('.attach').length+1;

		$('<input type="file" class="attach" name="'+$last_input_name.replace($total-1, $total)+'"/>').insertBefore($(this).parent('div'));
	});

	$('.remove').click(function(){

		if ($(this).parent('li').hasClass('off')) {
			$(this).attr('title','Excluir').parent('li').removeClass('off');
			$('#remove_files_'+$(this).parent('li').attr('id')).remove();
		} else {
			$(this).attr('title','Cancelar').parent('li').addClass('off');
			$('<input type="hidden" name="remove_files[]" id="remove_files_'+$(this).parent('li').attr('id')+'" value="'+$(this).parent('li').attr('id')+'" />').appendTo('form');
		}
		return false;
	});

});