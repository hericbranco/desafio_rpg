$().ready(function(){

	$('.nome').blur(function(){
		$(this).parent('.container').prev('h3').html($(this).val());
	});

	$('.excluir').live('click', function(){
		if ($(this).parents('form').find('.sanfona').length > 1) {

			if ($(this).parent('div').parent('.container').find('.id').length == 1) {
				$('<input></input>').attr({
					'type' : 'hidden',
					'name' : 'id_to_remove[]'
				}).val($(this).parent('div').parent('.container').find('.id').val()).appendTo($(this).parents('form'));
			}

			$(this).parent('div').parent('.container').parent('.sanfona').remove();
		}
		return false;
	});
});