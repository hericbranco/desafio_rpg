$().ready(function(){
	$('.edit').live('click', function(){
		$tr = $(this).parents('tr');
		$id = $tr.attr('id');

		$tr.find('td:first .titulo').hide();
		$nome = $tr.find('td:first .titulo').html();

		$('<input type="text" name="nome" id="nome" value="'+$nome+'" style="position:relative; padding:5px !important; top:0; left:0" /><input type="hidden" name="id" value="'+$id+'"/>').insertBefore($tr.find('td:first').children('.qtd'));

		$tr.find('.edit').addClass('save').attr('title','Salvar').removeClass('edit');
		$tr.find('.delete').addClass('remove');

		$('.remove').not($tr.find('.remove')).each(function(){
			$(this).click();
		});

		return false;
	});

	$('.save').live('click', function(){
		$(this).parents('form').submit();
		return false;
	});

	$('.remove').live('click', function(){
		$tr = $(this).parents('tr');

		$tr.children('td:first').children('.titulo').show();
		$tr.children('td:first').children('input').remove();

		$tr.find('.save').removeClass('save').attr('title', 'Editar').addClass('edit');
		$tr.find('.remove').removeClass('remove');

		return false;

	});

	$('.delete').live('click', function(){
		$tr = $(this).parents('tr');
		$texto_qtd_publicado = 'Esta categoria possui '+$tr.find('.qtd').html().replace('(', '').replace(')', '')+'. Tem certeza que deseja exclui-la?';
		var answer = confirm($texto_qtd_publicado)
		if (!answer){
			return false;
		}
	});

});