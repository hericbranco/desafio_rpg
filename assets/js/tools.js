/**
 * Prompt
 */
function promptAlert(msg, nome, acao){
	if(typeof(nome)== 'undefined'){
		nome = " ";
	}
	$.prompt(nome + '<div>'+msg+'</div>', {
		buttons:{'Ok':true},
		prefix:'colsJqi',
		focus:0,
		submit:function(v,m,f){
			if(acao=='reload'){
				window.location.reload(true);
				return false;
			} else{
				$.prompt.close();
			}
		}
	});
	return false;
}

/**
 * Prompt
 */
function promptAlertFechar(msg, nome, acao){
	if(typeof(nome)== 'undefined'){
		nome = " ";
	}
	$.prompt(nome + '<div>'+msg+'</div>', {
		buttons:{'Fechar':false},
		prefix:'colsJqi',
		focus:0,
		submit:function(v,m,f){
			if(acao=='reload'){
				window.location.reload(true);
				return false;
			} else{
				$.prompt.close();
			}
		}
	});
	return false;
}

/**
 * Permite requisicoes via http(Ajax)
 */
function httpRequest(urlDo, dadosForm){

	var resp;

	var ajax = $.ajax({
		type: 'POST',
		url: urlDo,
		data: dadosForm,
		async: false,
		beforeSend: function(){
			$('html').css('cursor', 'wait');
			//$.blockUI({ css: {}, message: "<div style='color: #FFFFFF;'><img alt='Aguardando...' src='" + assets + "img/ajax-loader.gif' /></div>" });
			//$.blockUI({ css: {}, message: "<div style='color: #FFFFFF;'></div>" });
		},
		success: function(resposta){
			$('html').css('cursor', 'auto');
			//$('html').unblock();
			resp = resposta;
		}
	});

	return resp;

}