$().ready(function(){
	$('form').submit(function(){
		$submit = true;
		$(this).find('input:visible, select:visible, textarea:visible').each(function(){
			if ($(this).prev('label').find('.red').html() != null) {
				if ($(this).val() == '') {
					$submit = false;
				}
			}
		});

		if ($submit == false) {
			//alert('Preencha os campos obrigatórios');
			$('.n_error').not('.verify_error').remove();
			$('form').before('<div class="n_error"><p>Por favor, preencha os campos obrigatórios</p></div>');

			return false;
		}
	});
});