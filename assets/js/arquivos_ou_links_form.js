$().ready(function(){
	$('.dv_link').hide();
	$('.dv_arquivo').hide();
	$('#btn_add_file').remove();


	$('#type').change(function(){
		$('.dv_link').hide();
		$('.dv_arquivo').hide();
		if ($(this).val() == 'Link') {
			$('.dv_link').show();
		} else if ($(this).val() == 'Arquivo') {
			$('.dv_arquivo').show();
		}
	});

	$('form').submit(function(){
		$submit = true;
		$(this).find('input:visible, select:visible, textarea:visible').each(function(){
			if ($(this).prev('label').find('.red').html() != null) {
				if ($(this).val() == '') {
					if ($(this).attr('type') == 'file' && $(this).parents('.element').find('#lista-arquivos').children('li').not('.off').length == 1) {
						$submit = true;
					} else {
						$submit = false;
					}
				}
			}
		});

		if ($submit == false) {
			//alert('Preencha os campos obrigatórios');
			$('.n_error').not('.verify_error').remove();
			$('form').before('<div class="n_error"><p>Por favor, preencha os campos obrigatórios</p></div>');

			return false;
		}
	});


	if ($('#type').val() != '') {
		$('#type').change();
	}



});