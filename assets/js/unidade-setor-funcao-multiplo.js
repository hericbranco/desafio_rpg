$().ready(function(){
	$('#btn_add_unidade').click(function(){
		$div_unidade = $('.div_unidade_setor_funcao').first().clone();

		$div_unidade.find('select, input').not('.id_unidade').each(function(){
			$(this).addClass('disabled').val('');
			if($(this).is("select")) {
				$(this).html('<option value="">'+$default_value+'</option>');
			} else {
				$(this).val('');
			}
		});

		$(this).parent().before($div_unidade);

	});

	$('.disabled').live('focus', function(){
		$(this).blur();
	});

	$default_value = 'Todos';
	$('.id_setor, .id_funcao').addClass('disabled');

	$('.id_unidade').live('change', function(){
		$id_unidade = $(this);
		$id_setor = $id_unidade.parents('.div_unidade_setor_funcao').find('.id_setor');
		$id_funcao = $id_setor.parents('.div_unidade_setor_funcao').find('.id_funcao');

		//alert($id_setor.attr('id'));
		if ($(this).val() != '') {
			$.ajax({
				url		: base_url + 'perfil/return_setor',
				data		: 'id_unidade='+$(this).val(),
				dataType	: 'json',
				async		: false,
				success	: function(arrObjSetor) {
					//alert(arrObjSetor);
					$id_setor.html('<option value="">'+$default_value+'</option>');
					$id_funcao.html('<option value="">'+$default_value+'</option>').addClass('disabled');
					$.each(arrObjSetor, function(id_setor, nome) {
						$option = $('<option></option>').attr('value', id_setor).html(nome);
						$id_setor.append($option);

					});
					$id_setor.val($id_setor.next('input').val()).removeClass('disabled').change();
					$id_setor.next('input').remove();
				}
			});
		} else {
			$id_setor.html('<option value="">'+$default_value+'</option>').addClass('disabled');
			$id_funcao.html('<option value="">'+$default_value+'</option>').addClass('disabled');
		}
	});

	$('.id_setor').live('change', function(){
		$id_setor = $(this);
		$id_funcao = $id_setor.parents('.div_unidade_setor_funcao').find('.id_funcao');

		if ($(this).val() != '') {
			$.ajax({
				url		: base_url + 'perfil/return_funcao',
				data		: 'id_setor='+$(this).val(),
				dataType	: 'json',
				async		: false,
				success	: function(arrObj) {
					$id_funcao.html('<option value="">'+$default_value+'</option>');
					$.each(arrObj, function(id, nome) {
						$option = $('<option></option>').attr('value', id).html(nome);
						$id_funcao.append($option);
					});
					$id_funcao.val($id_funcao.next('input').val()).removeClass('disabled');
					$id_funcao.next('input').remove();
				}
			});
		} else {
			$id_funcao.html('<option value="">'+$default_value+'</option>').addClass('disabled');
		}
	});

	$('.id_unidade').each(function(){
		if ($(this).val() != '') {
			$(this).change();
		}
	});
});
