$().ready(function(){

	$('.sanfona .container').hide();
	$('.sanfona h3').live('click', function(){
		$(this).toggleClass('open').next('.container').stop().slideToggle(500).parent().toggleClass('open');
		return false;
	});

	$('.excluir').not('.btn').live('click', function(){
		if ($(this).parents('.sanfona').parent('.container').find('.sanfona').length > 1) {

			if ($(this).parent('div').parent('.container').find('.id').length == 1) {
				$('<input></input>').attr({
					'type' : 'hidden',
					'name' : 'id_to_remove[]'
				}).val($(this).parent('div').parent('.container').find('.id').val()).appendTo($(this).parents('form'));
			}

			$(this).parent('div').parent('.container').parent('.sanfona').remove();
		}
		return false;
	});

	$.fn.duplicaContent = function() {

		$div = $(this).parent('div').prev('.sanfona').clone();

		$div.find('.id, .img').remove();

		//Abrir item criado
		$div.addClass('open').find('.container').show();

		$div.find('.enviar_img').html('Enviar imagem: <span class="red">(obrigatório)</span>');

		$titulo = $div.find('h3').addClass('open').html();

		$arrNumberTitulo = $titulo.match(/(\d+)$/);

		$titulo = $titulo.replace(/(\d+)$/, parseFloat($arrNumberTitulo[0])+1);
		$div.find('h3').html($titulo);

		$div.find('input, select, textarea').each(function(){
			if ($(this).attr('type') == 'file') {
				$arrNumberInput = $(this).attr('name').match(/_(\d+)_/);
				$number = parseFloat($arrNumberInput[1])+1;
				$new_name = $(this).attr('name').replace(/_(\d+)_/, '_'+$number+'_');
			} else {
				$arrNumberInput = $(this).attr('name').match(/\[(\d+)\]/);
				$number = parseFloat($arrNumberInput[1])+1;
				$new_name = $(this).attr('name').replace(/\[(\d+)\]/, '['+$number+']');
			}
			$(this).val('').attr('name', $new_name);
		});

		$div.insertBefore($(this).parent('div'));
	};

	$('.add').not('button').click(function(){
		$(this).duplicaContent();
		return false;
	});

});